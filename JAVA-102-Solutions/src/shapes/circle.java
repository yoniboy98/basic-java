package shapes;

public class circle extends shape {
protected static double radius;

public circle(String color, boolean Filled, double radius) {
	super(color, Filled);
	this.radius=radius;
	
}
public circle() {
	super();
	this.radius = 1.0;
	
}
@Override
public double getArea() {
	return radius*radius*Math.PI;
}
@Override
public double getperimeter () {
	return Math.PI*2*radius;
}
public double getRadius() {
	return radius;
}
public void setRadius(double radius) {
	this.radius = radius;
}
@Override
public String toString() {
	return "circle [radius=" + radius + ", getArea()=" + getArea() + ", getperimeter()=" + getperimeter()
			+ ", toString()=" + super.toString() + "]";
}
@Override
public double getPerimeter() {
	// TODO Auto-generated method stub
	return 0;
}
@Override
public double geArea() {
	// TODO Auto-generated method stub
	return 0;
}

}




