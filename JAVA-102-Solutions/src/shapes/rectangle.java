package shapes;

public class rectangle extends shape {
	protected double width;
	protected double lenght;

	public rectangle() {
		super();
		this.width = 1.0;

		this.lenght = 1.0;
		
	}

	public rectangle(String color, boolean Filled, double width, double lenght) {
		super(color, Filled);
		this.width = width;
		this.lenght = lenght;
	}
	
	public double getArea() {
		return lenght*width;
	}
	public double getperimeter() {
		return (lenght+width) *2;
	}

	public double getWidth() {
		return width;
	}

	@Override
	public String toString() {
		return "rectangle [width=" + width + ", lenght=" + lenght + ", getArea=" + getArea() + ", getperimeter="
				+ getperimeter() + ", toString=" + super.toString() + "]";
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLenght() {
		return lenght;
	}

	public void setLenght(double lenght) {
		this.lenght = lenght;
	}

	@Override
	public double getPerimeter() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double geArea() {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
