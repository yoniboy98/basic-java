package shapes;

public class Resizeablesquare extends square implements ResizeableInterface{
	
	public Resizeablesquare(String color, boolean Filled, double side) {
		super(color,Filled, side);
}

	@Override
	public void resize(int percent) {
	
		System.out.println("old side" + super.getSide());
		super.setSide(super.getSide()+super.getSide()*4);
		System.out.println("new side" + super.getSide());
		
	}}
