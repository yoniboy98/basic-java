package shapes;

public abstract class shape implements Interface {
	protected static String color;
	protected static boolean Filled;


public shape(String color, boolean Filled) {
	super();
	this.color=color;
	this.Filled=Filled;
	
}

public shape() {
	this.color = "green";
	this.Filled=true;
}
public abstract double getperimeter();
public abstract double getArea();

public String getColor() {
	return color;
}

public void setColor(String color) {
	this.color = color;
}

public boolean getFilled() {
	return Filled;
}

public void setFilled(boolean filled) {
	this.Filled = filled;
}

@Override
public String toString() {
	return "shape [color=" + color + ", Filled=" + Filled + "]";
}}