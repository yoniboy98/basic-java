package shapes;

public class ResizeableCircle extends circle implements ResizeableInterface {
	
 public ResizeableCircle() {
		super(color,Filled,radius);	
}

 
@Override
public void resize(int percent) {
	System.out.println("old radius" + super.getRadius());
	super.setRadius(super.getRadius() + super.getRadius() * percent /100);
	System.out.println("new radius" + super.getRadius());
	
}
}

