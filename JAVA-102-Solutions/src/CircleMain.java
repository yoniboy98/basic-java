
public class CircleMain {

	public static void main(String[] args) {
		
		Circle point1 = new Circle(5, 10, 7);
		Circle point2 = new Circle(8, 15, 4);
	
		
		System.out.println(point1);
		System.out.println(point1.getCenter());
		System.out.println(point1.getRadius());
		
		System.out.println(point2);
		System.out.println(point2.getCenter());
		System.out.println(point2.getRadius());
	}

	

}
