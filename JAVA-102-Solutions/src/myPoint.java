
public class myPoint {
	private int x;
	private int y;

	public myPoint() {
		x = 0;
		y = 0;

	}

	public myPoint(int x, int y) {
		this.x = x;
		this.y = y;

	}

	public void setX(int x) {
		this.x = x;

	}

	public int getX() {
		return x;

	}

	public void setY(int y) {
		this.y = y;
	}

	public int getY() {
		return this.y;
	}

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public String toString() {
		return "(" + this.x + ", " + this.y + ")";
	}

	public double distance(int x, int y) {
		int distanceX = this.x - x;
		int distanceY = this.y - y;
		return (double) Math.abs(Math.sqrt(distanceX * distanceX) + (distanceY * distanceY));
	}

	public double distance(myPoint mp2) {
		int distanceX = this.x - mp2.getX();
		int distanceY = this.y - mp2.getY();
		return (double) Math.abs(Math.sqrt(distanceX * distanceX) + (distanceY * distanceY));
	}
	
}
