
public class Account {
private String code; 
private double saldo;

public void account(String code, double saldo) {
	this.code=code;
	this.saldo=saldo;
}
public Account (double saldo) {
	this.saldo=0;
}

public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}

public double getSaldo() {
	return saldo;
}

public void setSaldo(double saldo) {
	this.saldo = saldo;
}

@Override
public String toString() {
	return "account [code=" + code + ", saldo=" + saldo + "]";
}
}
