
public class Address {
private String straat;
private String huisnummer;
private int postcode;
private String gemeenten;
private String land;

public Address (String straat, String huisnummer, int postcode, String gemeenten, String land) {
	this.straat = straat;
	this.huisnummer = huisnummer;
	this.postcode = postcode; 
	this.gemeenten = gemeenten;
	this.land = land;
}
public void setStraat(String straat) {
	this.straat = straat;
}
public String getStraat() {
	return this.straat;
	
}
public void setHuisnummer(String huisnummer) {
	this.huisnummer = huisnummer;
	
}
public String getHuisnummer() {
	return this.huisnummer;
}
public void setPostcode(int postcode) {
	this.postcode = postcode;
}
public int getPostcode() {
	return this.postcode;
}
public void setgemeenten(String gemeenten) {
	this.gemeenten = gemeenten;
}
public String getgemeenten() {
return	this.gemeenten;
}
public void setland(String land) {
	this.land = land;
	}
public String getland() {
	return this.land;
	
}
}
