package education.basics;

import java.math.BigInteger;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

public class OefeningKlas {
	private static final double KILOMETERS_PER_MILE = 1.609;
	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		// welkom();
		// zelfreflectie();
		// displayPatterns();
		 //optellen();
		// helloDate();
		// bigInteger();
		// hogerlager();
		// zelfreflectie();
		// CalculateTwoValues();
		 //versleutelen();
		// probeersel();
		// cookies();
		// stringen();
		// condities ();
		// salary ();
		// takscalculator();
		// raadgame();
		// displayNumberPiramid(100);
		// intresten();
		//gradeStatics();
		// MilesToKilo();
		// bmi();
		// population();
		//Formatloops();
		//StringChar();
		// namespel();
	}

//---------------------------------------------------------------------------
	private static void welkom() {
		System.out.println("Welcome to java");
		System.out.println("welcome to computer sience");
		System.out.println("programming is fun");

	}

	// ---------------------------------------------------------------------------
	private static void displayPatterns() {
		System.out.println("J A V V A");
		System.out.println("J A A V V A A");
		System.out.println("J J AAAAA V V AAAA");
		System.out.println("J J A A V A A");

	}

	// ---------------------------------------------------------------------------
	private static void optellen() {
		System.out.println(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9);

	}

	// ---------------------------------------------------------------------------
	private static void avarage() {
		double time = 0.7583;
		int distance = 14;
		System.out.println((distance / time) * 0.621371192);

	}

	// ---------------------------------------------------------------------------
	private static void helloDate() {
		LocalDate date = LocalDate.now();
		System.out.println("hello world  " + date);

	}

	// ---------------------------------------------------------------------------
	private static void bigInteger() {
		BigInteger big = new BigInteger("2000000000");
		BigInteger uitkomst = new BigInteger("2000000000");
		uitkomst = big.add(big);
		System.out.println(uitkomst);

	}

	// ---------------------------------------------------------------------------
	private static void hogerlager() {
		System.out.println("byte minimum value = " + Byte.MIN_VALUE);
		System.out.println("byte max value = " + Byte.MAX_VALUE);
		System.out.println("short minimum = " + Short.MIN_VALUE);
		System.out.println("short max = " + Short.MAX_VALUE);
		System.out.println("long min = " + Long.MIN_VALUE);
		System.out.println("int max = " + Long.MAX_VALUE);
	}

	// ---------------------------------------------------------------------------
	private static void zelfreflectie() {

		Scanner scan = new Scanner(System.in);
		Scanner scan1 = new Scanner(System.in);
		int user_input = scan.nextInt();
		int user_input_ok = scan1.nextInt();
		if (user_input > user_input_ok) {
			System.out.println(user_input + " = is larger");
		}

		if (user_input < user_input_ok) {
			System.out.println(user_input_ok + " = is larger");
		} else if (user_input == user_input_ok) {
			System.out.println("dit kan niet");

		}
	}

	// ---------------------------------------------------------------------------
	private static void CalculateTwoValues() {
		int value01 = 4;
		int value02 = 178;
		int value03 = 3;
		System.out.println("lol =" + (value01 + value02 + value03));
	}

	// ----------------------------------------------------------------------------
	private static void versleutelen() {
	

		System.out.println("geef je 4 cijfers");

		String input = scan.nextLine();
		input = input.substring(0, 4);
		int getal1 = Integer.parseInt(input.substring(0, 1));
		int getal2 = Integer.parseInt(input.substring(1, 2));
		int getal3 = Integer.parseInt(input.substring(2, 3));
		int getal4 = Integer.parseInt(input.substring(3, 4));

		getal1 = (getal1 + 6) % 10;
		getal2 = (getal2 + 6) % 10;
		getal3 = (getal3 + 6) % 10;
		getal4 = (getal4 + 6) % 10;

		System.out.println("je cijfers zijn nu = " + getal3 + getal4 + getal1 + getal2);

		boolean geldigAntwoord;
		do {
			System.out.println("wenst u decryptie?");
			String letter = scan.next();
			if (letter.contentEquals("ja")) {
				int getal1decryptie = (getal1 + 4) % 10;
				int getal2decryptie = (getal2 + 4) % 10;
				int getal3decryptie = (getal3 + 4) % 10;
				int getal4decryptie = (getal4 + 4) % 10;
				System.out.println(
						"hier is decryptie " + getal1decryptie + getal2decryptie + getal3decryptie + getal4decryptie);
				System.out.println(student.DOUBLE_LINE);
				geldigAntwoord = true;
			} else if (letter.contentEquals("nee")) {
				geldigAntwoord = true;
				System.out.println("je cijfers zijn nu weer = " + getal3 + getal4 + getal1 + getal2);
				System.out.println(student.DOUBLE_LINE);
			} else {
				geldigAntwoord = false;
				System.out.println("fuck you je moet ja of nee zetten");

			}

		} while (!geldigAntwoord);
	}

//-------------------------------------------------------------------------------------

	private static void probeersel() {
		int a = 1;
		while (a < 100) {
			System.out.println("leuke tekst mijn getal is " + a);
			a = a * 2;

		}

		System.out.println("je getal is te hoog " + a);
//---------------------------------------------------------------------------------------------
	}

	private static void cookies() {
		Scanner scan = new Scanner(System.in);
		System.out.println("how many cookies you want");
		int userInput = scan.nextInt();
		double cupsOfSugar = 2;
		double cupsOfButter = 1;
		double cupsOfFlour = 2.75;

		double x = (double) userInput / 48;

		System.out.println("cups of sugar " + x * cupsOfSugar);

		System.out.println("cups of butter " + x * cupsOfButter);

		System.out.println("cups of flower " + x * cupsOfFlour);

	}
//------------------------------------------------------------------------------------------

	private static void stringen() {
		Scanner scan = new Scanner(System.in);
		Scanner scan1 = new Scanner(System.in);
		System.out.print("firstname? ");
		String user_input = scan.nextLine();
		System.out.print("last name? ");
		String user_input1 = scan.nextLine();
		System.out.println(user_input1.toUpperCase());
		System.out.println(student.DOUBLE_LINE);
		double result = Math.random();
		System.out.println(result * 100);

		int price = 45;
		int quantitiy = 5;
		int taxrate = 21;
		System.out.println("total price = " + price * quantitiy / 100 * taxrate);

	}

//-----------------------------------------------------------------------------------------------------------------------------------
	private static void condities() {
		int user_input = scan.nextInt();
		int mark = user_input;
		if (mark >= 50) {
			System.out.println("pass");

		} else {
			System.out.println("fail");
		}
		switch (mark) {
		case 90:
			System.out.println("mooi!");
			break;
		case 80:
			System.out.println("goed!");
			break;
		case 70:
			System.out.println("oke!");
			break;
		case 60:
			System.out.println("matig");
			break;
		case 50:
			System.out.println("juist erdoor!");
			break;
		case 40:
			System.out.println("sorry");
			break;
		case 30:
			System.out.println("kan veel beter");
			break;
		case 20:
			System.out.println("niet goed");
			break;
		case 10:
			System.out.println("neen");
			break;
		default:
			System.out.println("moet nog gedaan worden");
		}
	}

	private static void salary() {
		System.out.println("hoeveel uren heb je gewerkt?");
		int user_input = scan.nextInt();
		int uren = user_input;
		double uurloon = 10.00; // *1.5 = 15
		double extra = ((user_input - 40) * 5);
		if (user_input >= 40) {
			System.out.println((uurloon * user_input + extra) + " is je brutoloon");
		} else
			System.out.println("weekloon = " + user_input * uurloon + " is je brutoloon");
	}

	private static void takscalculator() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your income: ");
		String userIncome = sc.nextLine();

		double uIncome = Double.parseDouble(userIncome);
		double socialSecurity = (uIncome / 100 * 13.7); // 13.7%
		double taxableIncome = (uIncome - socialSecurity);
		double taxation = (int) (taxableIncome / 100 * 21); // normaal 25 maar heb zelf 21% genomen
		double netIncome = (taxableIncome - taxation);

		System.out.println("social security: " + socialSecurity);
		System.out.println("taxable income: " + taxableIncome);
		if (taxableIncome < 8680) {
			System.out.println("taxation: " + taxation);
		} else {
			System.out.println("taxation: " + (double) taxation);
		}
		System.out.println("netto income: " + netIncome);

	}

	private static void raadgame() {
		System.out.println("geef je waarde in van 1 t.e.m 10");
		Scanner scan = new Scanner(System.in);
		int raadgetal = (int) (Math.random() * 10);
		int counter = 0;
		boolean geraden = false;
		while (!geraden) {
			counter++;
			if (counter == 4) {
				System.out.println("verloren");
				break;
			}
			System.out.print(" geef je waarde in ");
			int getal = scan.nextInt();
			if (getal < raadgetal) {
				System.out.println("zoek hoger");

			} else if (getal > raadgetal) {
				System.out.println("zoek lager");
			} else {
				System.out.println("you won " + raadgetal);
				geraden = true;
			}

		}
	}

	private static void displayNumberPiramid(int limit) {
		for (int i = 1; i <= limit; i++) {
			for (int j = 1; j <= limit; j++) {
				System.out.print(i * j + "\t");
			}
			System.out.print("\n\r");
		}
	}

	private static void intresten() {
		Scanner input = new Scanner(System.in);

		System.out.println("Geef het maandelijkse bedrag in dat u wenst te sparen in euro: ");
		double sparen = input.nextDouble();

		System.out.println("Geef de jaarlijkse interest in %: ");
		double intrest = input.nextDouble();
		double intrestPM = (intrest / 100) / 12;
		double[] maanden = new double[7];
		maanden[0] = (sparen);
		maanden[1] = (maanden[0]) * (1 + intrestPM);
		maanden[2] = (maanden[0] + maanden[1]) * (1 + intrestPM);
		maanden[3] = (maanden[0] + maanden[2]) * (1 + intrestPM);
		maanden[4] = (maanden[0] + maanden[3]) * (1 + intrestPM);
		maanden[5] = (maanden[0] + maanden[4]) * (1 + intrestPM);
		maanden[6] = (maanden[0] + maanden[5]) * (1 + intrestPM);

		System.out.println("aantal euro gespaard: " + maanden[0] + " na " + 0 + " maanden");
		for (int index = 1; index < maanden.length; index++) {

			System.out.println(
					"aantal euro gespaard: " + student.afronden(maanden[index], 2) + " na " + index + " maanden");
		}

	}

	private static void gradeStatics() {
		Scanner scan = new Scanner(System.in);
		int aantalStudenten;
		int score;
		int som = 0;
		int maxscore = 0;
		int minscore = Integer.MAX_VALUE;
		System.out.print("hoeveel studenten zijn er? ");
		aantalStudenten = scan.nextInt();
		int[] punten = new int[aantalStudenten];
		for (int i = 0; i < aantalStudenten; i++) {
			System.out.print("geef je score in van student " + (i + 1) + "  ");
			score = scan.nextInt();
			punten[i] = score;
			if (score == -1) {
				System.out.println("canceled by the user");
				break;
			}
		}
		// gemiddelde
		for (int punt : punten) {

			som += punt;

		} // maxwaarde
		System.out.println("het gemiddelde is = " + (double) som / aantalStudenten);
		for (int punt : punten) {
			if (punt > maxscore) {
				maxscore = punt;
			}
		}
		System.out.println("hoogste score is = " + maxscore);
		// minwaarde
		for (int punt : punten) {
			if (punt < minscore) {
				minscore = punt;
			}
		}
		System.out.println("laagste score is = " + minscore);
	
		}
		
	

	private static void MilesToKilo() {
		Scanner scan = new Scanner(System.in);
		System.out.println("geef de miles in");
		int miles = scan.nextInt();
		double kilometers;
		System.out.println(miles / KILOMETERS_PER_MILE + " kilometers");

	}

	private static void bmi() {
		Scanner scan = new Scanner(System.in);
		Scanner scan1 = new Scanner(System.in);

		System.out.println("hoeveel weeg je?");
		double user_input = scan.nextDouble();
		double weight = user_input;
		System.out.println("wat is je lengte? (in centimeter)");
		double user_height = scan.nextDouble();
		double height = user_height;
		double bmi = user_input / (user_height * user_height);
		System.out.println("je gewicht in kilo's is " + user_input);
		System.out.println("je lengte is " + user_height + " cm");
		System.out.println("je bmi is = " + bmi);

		if (bmi <= 18.5) {
			System.out.println("je bmi is te laag");
		} else if (bmi > 18.5 && bmi < 24.9) {
			System.out.println("je bent gezond");
		} else if (bmi > 25) {
			System.out.println("je bmi is te hoog");
		}

	}

	private static void population() {
		System.out.println("welk jaar wil je zien? max 5");
		int aantalJaren = scan.nextInt();
		long currentPopulation = 312032486;
		int begroting = 2628000;
		int jaren = begroting * aantalJaren;
		if (aantalJaren == 4) {
			System.out.println("na 4 jaar " + (currentPopulation + jaren));
		} else if (aantalJaren == 3) {
			System.out.println("na 3 jaar " + (currentPopulation + jaren));
		} else if (aantalJaren == 1) {
			System.out.println("na 1 jaar " + (currentPopulation + jaren));
		} else if (aantalJaren == 5) {
			System.out.println("na 5 jaar " + currentPopulation + jaren);
		} else if (aantalJaren == 2) {
			System.out.println("na 2 jaar " + currentPopulation + jaren);
		}

	}

	public static void Formatloops() {

		int i = 42;
		String str = "" + i;
		System.out.println(i);
	}

	private static void StringChar() {
		String input = scan.nextLine();

		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			System.out.println("\t\n" + c);
		}
	}


public static void namespel() {
	System.out.println("dit is de name game ik zal je vertellen of je naam hetzelfde is als de vorige");
	String name = scan.nextLine();

	System.out.println("geef nu je 2de naam");
	String nametwo = scan.nextLine();
	if (name.contentEquals(nametwo)) {
		System.out.println("ok same");

	} else {
		System.out.println("niet same");
		
		namespel();

	}
	
	
}
}