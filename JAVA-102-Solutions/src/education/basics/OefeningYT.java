package education.basics;

import java.util.Random;
import java.util.Scanner;

public class OefeningYT {

	public static void main(String[] args) {
		//whileLoop();
		// doWhileloop ();
		// arrays();
		// forLoop();
		// string();
		// objects();
		// accesModifiers();
		// testen ();
		// testen2();
		// lol ();
		// eraayysss ();

	}

	private static void whileLoop() {
		int a = 1;

		while (a <= 10) {
			
			System.out.println(a);
			a++;
		}
	}

	// ------------------------------------------------------------------

	private static void doWhileloop() {
		int a = 1;

		while (a <= -1) {

			System.out.println(a);
			a++;
			System.out.println("------------------------------------------------------------------------------");
		}
		int b = 0;
		do {
			System.out.println(b);
			b++;
		} while (b <= -1);
	}

//-----------------------------------------------------------------------------------
	private static void arrays() {
		int[] myintarray = { 100, 220, 541, 147, 547 };
		int index = 0;
		while (index < 5) {
			System.out.println(myintarray[index]);
			index++;
		}

	}
//-----------------------------------------------------------------------------------------------

	private static void forLoop() { // unit ; expression ; increment
		int[] myintarray = { 100, 31, 26, 48, 52 };

		/*
		 * int index=0; while(index < 5) {
		 * 
		 * System.out.println(myintarray[index]); index++;}
		 */

		/*
		 * for (int index=0 ; index < 5 ; index++) {
		 * System.out.println(myintarray[index]);
		 */

		for (int lollen : myintarray) {
			System.out.println(lollen);
		}
	}

//-------------------------------------------------------------------------------------------
	private static void string() {
		String mystring = "hello world e";
		int mystringlenght = mystring.length();
		String mystringcase = mystring.toUpperCase();

		System.out.println(mystring.replace('e', 'a'));
		System.out.println(mystring.indexOf('o'));
		System.out.println(mystringlenght);
		System.out.println(mystringcase);
	}

//-------------------------------------------------------------------------------------------------
	private static void objects() {

		/*
		 * student tom = new student(); //tom --> object or instance tom.id = 2;
		 * tom.name = "tom"; tom.age = 14;
		 * 
		 * System.out.println(tom.name + " is " + tom.age + " years old");
		 */
	}
//--------------------------------------------------------------------------------------------------
// method overloading 

	/*
	 * System.out.println(add(1,36)); System.out.println(add(5.658,40.247));
	 * System.out.println(add("hahah ", "loser"));
	 */

	private static int add(int a, int b) {
		return (a + b);
	}

	private static double add(double a, double b) {
		return (a + b);
	}

	private static String add(String a, String b) {
		return (a + b);
	}

//-----------------------------------------------------------------------------------------------------

	private static void accesModifiers() {
		/*
		 * class package subclass world public y y y y protected y y y n private y n n n
		 * no modifier y y n n
		 */

	}

	private static void testen() {
		double amount;
		double prinicipel = 10000;
		double rate = .01;

		for (int day = 1; day <= 20; day++) {
			amount = prinicipel * Math.pow(1 + rate, day);
			System.out.println(day + "+" + amount);

		}
	}

	private static void testen2() {
		Random rand = new Random();
		int[] freq = new int[7];
		for (int roll = 1; roll <= 1000; roll++) {

			++freq[1 + rand.nextInt(6)];
		}

		System.out.println("face\tFreq");
		for (int face = 1; face < freq.length; face++) {

			System.out.println(face + "\t" + freq[face]);
		}
	}

	private static void lol() {
		int[] bucky = { 3, 4, 5, 6, 7 };
		change(bucky);
		for (int y : bucky)
			System.out.println(y);

	}

	public static void change(int x[]) {
		for (int counter = 0; counter < x.length; counter++) {
			x[counter] += 5;
		}
	}

	private static void eraayysss() {
		int firstarray[][] = { { 8, 9, 10, 11 }, { 12, 13, 45, 65 } };
		int secondaraay[][] = { { 30, 31, 32, 33 }, { 43 }, { 4, 5, 6 } };
		System.out.println("this is the first array");
		display(firstarray);
		System.out.println("this is the second array");
		display(secondaraay);
	}

	public static void display(int x[][]) {
		for (int row = 0; row < x.length; row++) {
			for (int columm = 0; columm < x[row].length; columm++) {
				System.out.print(x[row][columm] + "\t");
			}
			System.out.println();
		}
	}
	/*
	 * private static int average (int...numbers) { int total=0; for(int x :numbers)
	 * { total+=x; return total/numbers.length; } }
	 */

}
