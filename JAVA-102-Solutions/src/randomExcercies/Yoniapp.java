package randomExcercies;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

public class Yoniapp extends JFrame {
	
	private JTextField item1;
	private JTextField item2;
	private JTextField item3;
	private JPasswordField passwordField;
	
	public Yoniapp () {
		super("the Yoni app");
		setLayout(new FlowLayout());
		item1 = new JTextField(10);
		add(item1);
		item2 = new JTextField("enter text here");
		add(item2);
		
		passwordField = new JPasswordField ("mypass");
		add(passwordField);
		
		item3 = new JTextField("uneditbale", 20);
		item3.setEditable(false);
		add(item3);
		
		
		
		thehandler handler = new thehandler();
		item1.addActionListener(handler);
		item2.addActionListener(handler);
		item3.addActionListener(handler);
		passwordField.addActionListener(handler);
	}
	private class thehandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			String String = "";
			
			if(event.getSource()==item1)
				String = String.format("field 1: %s", event.getActionCommand());
			else if(event.getSource()==item2)
				String = String.format("field 2: %s", event.getActionCommand());
			else if(event.getSource()==item3)
				String = String.format("field 3: %s", event.getActionCommand());
			else if (event.getSource()==passwordField)
				String=String.format("password field is %s", event.getActionCommand());
JOptionPane.showMessageDialog(null, String);
		}
	}

}
