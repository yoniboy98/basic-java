import interfaces.Noisy;

public abstract class Animal implements Noisy {
	protected String gender;
	protected String name;


	public Animal(String gender, String name) {
		super();
		this.gender = gender;
		this.name = name;
	}

	
public Animal() {
	
}


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
