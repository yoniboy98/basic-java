package beehive;

import interfaces.Flying;

public class Bee extends Animal implements Flying {
	private String color;
	private double size;
	private double wingspan;

	public Bee(String color, double size, double wingspan, String gender) {
		super();
		this.color = color;
		this.size = size;
		this.wingspan = wingspan;
	}

	public Bee() {
		this.color = "black/yellow";
		this.size = 1.0;
		this.wingspan = 1.0;
			}
 @Override
	public void fly() {
		System.out.println("i fly ");

	}
 @Override
 public void makeNoice() {
	 System.out.println("lol");
 }

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public double getWingspan() {
		return wingspan;
	}

	public void setWingspan(double wingspan) {
		this.wingspan = wingspan;
	}
}
