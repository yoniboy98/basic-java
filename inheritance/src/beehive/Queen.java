package beehive;
import interfaces.Flying;

public class Queen extends Bee implements Flying {
private int minions;
private String food;
private int power;


public Queen(int minions, String food, int power, String color, int size, int wingspan, String gender) {
	super(color, size, wingspan, gender);
	this.minions = minions;
	this.food = food;
	this.power = power;


	
}


public Queen() {
this.minions = 5;	
this.power = 2;
this.food = "appel";

}

public void layegg() {
	System.out.println("legt ei");
}
public void commandbee() {
	System.out.println("commando man");
	
}
public int getMinions() {
	return minions;
}
public void setMinions(int minions) {
	this.minions = minions;
}
public String getFood() {
	return food;
}
public void setFood(String food) {
	this.food = food;
}
public int getPower() {
	return power;
}
public void setPower(int power) {
	this.power = power;
}

}
