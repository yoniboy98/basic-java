package beehive;
import interfaces.Flying;
import interfaces.Noisy;

public class Drone extends Bee implements Noisy, Flying {
	private int tricks;
	private int endurance;
	private String trickscolor;
	
public Drone() {
	
}
	
	@Override
	public void makeNoice() {
		System.out.println("lol");
		
	}
	@Override
public void fly() {
		System.out.println("i fly");
	
}
		
	public Drone(String color, double size, double wingspan, String gender, int tricks, int endurance,
			String trickscolor) {
		super(color, size, wingspan, gender);
		this.tricks = tricks;
		this.endurance = endurance;
		this.trickscolor = trickscolor;

	
	}
	public int getTricks() {
		return tricks;
	}
	public void setTricks(int tricks) {
		this.tricks = tricks;
	}
	public int getEndurance() {
		return endurance;
	}
	public void setEndurance(int endurance) {
		this.endurance = endurance;
	}
	public String getTrickscolor() {
		return trickscolor;
	}
	public void setTrickscolor(String trickscolor) {
		this.trickscolor = trickscolor;
	}
	public void fertilizeQueen() {
		
	}
	public void visitHive() {
		System.out.println("omg visiet givee");
	}
		



}
