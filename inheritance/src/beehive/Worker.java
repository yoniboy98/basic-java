package beehive;

import interfaces.Flying;

public class Worker extends Bee implements Flying{
	private double capacity;
	private double load;
	private int strength;
	
	



	public Worker(String color, double size, double wingspan, String gender, double capacity, double load,
			int strength) {
		super(color, size, wingspan, gender);
		this.capacity = capacity;
		this.load = load;
		this.strength = strength;

	}

	public Worker() {
		// TODO Auto-generated constructor stub
	}

	public void gatherNectar () {
		System.out.println("gathering nectar ");
		
	}
	public void repairHive() {
		System.out.println("repair hive");
	}


	public double getCapacity() {
		return capacity;
	}


	public void setCapacity(double capacity) {
		this.capacity = capacity;
	}


	public double getLoad() {
		return load;
	}


	public void setLoad(double load) {
		this.load = load;
	}


	public int getStrength() {
		return strength;
	}


	public void setStrength(int strength) {
		this.strength = strength;
	}
	public void Flying() {
		
	}
}
