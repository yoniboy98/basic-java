
import interfaces.Noisy;

public class Cat extends Animal implements Noisy {
private String name;


public Cat(String name, String gender) {
	super(gender, name);
	this.name = name;
	this.gender = gender;
}
public Cat() {
	
}


public String getName() {
	return name;
}


public void setName(String name) {
	this.name = name;
}
@Override
public void makeNoice() {

	System.out.println("test");
}


	
}


