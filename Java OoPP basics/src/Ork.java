
public class Ork extends GameObject {
private String wapen;


public Ork(int id, String name, int strenght, String wapen) {
	super(id, name, strenght);
	this.wapen = wapen;
}


public Ork() {
	
}

public String getWapen() {
	return wapen;
}
public void setWapen(String wapen) {
	this.wapen = wapen;
}

@Override
public String toString() {
	return "Ork [wapen=" + wapen + "]";
} 
}
