
public abstract class GameObject {
private int id;
private String name;
private int strenght;

public GameObject(int id, String name, int strenght) {
	super();
	this.id = id;
	this.name = name;
	this.strenght=strenght;
	}	
	
public GameObject() {
	
}




public int getId() {
	return id;
}



public void setId(int id) {
	this.id = id;
}



public String getName() {
	return name;
}



public void setName(String name) {
	this.name = name;
}



public int getStrenght() {
	return strenght;
}



public void setStrenght(int strenght) {
	this.strenght = strenght;
}


@Override
public String toString() {
	return "GameObject [id=" + id + ", name=" + name + ", strenght=" + strenght + "]";
}


}
