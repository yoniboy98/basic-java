

public class Wizard extends GameObject implements Moveable{
private int level;
private Point apoint;

public Wizard(int id, String name, int strenght, int level) {
super(id,name,strenght);
	this.level = level;

}
public Wizard() {

	
}
public int getLevel() {
	return level;
}
public Point getApoint() {
	return apoint;
}
public void setApoint(Point apoint) {
	this.apoint = apoint;
}
public void setLevel(int level) {
	this.level = level;
}
@Override
public String toString() {
	return "Wizard [level=" + level + "]";
}
@Override
public void MoveUp() {
	// TODO Auto-generated method stub
	
}
@Override
public void moveDown() {
	// TODO Auto-generated method stub
	
}
@Override
public void moveLeft() {
	// TODO Auto-generated method stub
	
}
@Override
public void moveRight() {
	// TODO Auto-generated method stub
	
}


}
