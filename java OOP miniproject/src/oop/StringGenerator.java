package oop;


import java.util.concurrent.ThreadLocalRandom;

public class StringGenerator {
	
	private static String personFirstNames[] = new String[] {"Steven","Maxime","Stijn","Yorick","Jeroen","Yoni","Seda","Moushine","Youssef","Adel"};
	private static String personLastNames[] = new String[] {"Esnol",
			"Ahriga",
			"Buelens",
			"Coeckelbergh",
			"El Amrani",
			"Lemrabet",
			"Lens",
			"Sunaert",
			"Tehraoui",
			"Van Caekenbergh",
			"Vindelinckx",
			"Wauters"};
	
	private static String positionsOnTheField[] = new String[] {"Keeper","Offense","Defence","Midfield","Bench"};

	public static String getRandomFirstName() {
		return personFirstNames[randomInteger(personFirstNames.length)];
	}
	
	public static String getRandomLastName() {
		return personLastNames[randomInteger(personLastNames.length)];
	}
	
	public static String getRandomFieldPositions() {
		return positionsOnTheField[randomInteger(positionsOnTheField.length)];
	}
	
	public static int randomInteger(int max) {
		return ThreadLocalRandom.current().nextInt(0, max);
	}
	
	
}


