package oop;

public class Trainer extends Person{
	private boolean fired;
	
	public Trainer() {
		super();
		this.fired = false;
	}

	public Trainer(int id, String firstName, String lastName, boolean fired) {
		super(id, firstName, lastName);
		this.fired = fired;
	}

	public boolean isFired() {
		return fired;
	}

	public void setFired(boolean fired) {
		this.fired = fired;
	}

	@Override
	public String toString() {
		return super.toString() + "\n\tTrainer [fired=" + fired + "]\n";
	}
	
	
}
