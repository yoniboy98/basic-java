package oop;

public class Player extends Person implements PlaySoccer{

	private String positionOnField;

	public Player() {
		super();
		this.positionOnField = "(niet gekend)";
	}

	public Player(int id, String firstName, String lastName, String positionOnField) {
		super(id, firstName, lastName);
		this.positionOnField = positionOnField;
	}

	@Override
	public String play() {
		return "geeft een pass";
	}

	public String getPositionOnField() {
		return positionOnField;
	}

	public void setPositionOnField(String positionOnField) {
		this.positionOnField = positionOnField;
	}

	@Override
	public String toString() {
		return super.toString() + "\n\tPlayer [positionOnField=" + positionOnField + "]\n";
	}
}
