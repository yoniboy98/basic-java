package oop;

import java.util.ArrayList;
import java.util.HashMap;

public class Collections {

	/*
	 * 	a)	In 2.java oop, you had to create a few classes. Now build a Team with a 11 players and a coach. Make sure that the id is incremental. 
	 * 		So for each player/coach created there is a unique id, Give them also a name, use the given class to create random first and last name. 
	 * 		Tip : use a loop for the players and add the coach as first element in the list. 
		b)	Use the method print to print out all the objects in the list. 
		c)	Now set the coach, fired on false and remove the coach from the team.
		d) 	Make a Map<String,ArrayList<Player>> where the key is the position of the player on the field and 
			find all the players for the same position and pass it as value.
 * 		e) Print the Map in printMap with first the position and than the values, use the tostring of player
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//create new team
		Team t = new Team();
		t.setId(0);
		t.setTeamName("RSC Anderlecht");
		t.setPerson(createPersonsForTeam());
		
		HashMap<String, ArrayList<Player>> positionsMap = findAllPlayersForPosition(t);
		printMap(positionsMap);
		//use team.setPersons() to set the arraylist of persons
		//
	}
	
	public static void removeCoachFromTeam(Team t) {
		ArrayList<Person> teamPeople = t.getPerson();
		
		for(int i = 0; i < teamPeople.size(); i++) {
			if(teamPeople.get(i) instanceof Trainer) {
				teamPeople.remove(i);
			}
		}
		
		t.setPerson(teamPeople);
	}
	
	public static ArrayList<Person> createPersonsForTeam(){
		ArrayList<Person> team = new ArrayList<>();
		for(int i = 0; i <= 11; i++) {
			if(i == 0) {
				Person p = new Trainer(i, StringGenerator.getRandomFirstName(), StringGenerator.getRandomLastName(), false);
				team.add(p);
			} else {
				team.add(createPlayer(i));
			}
		}
		return team;
	}
	
	public static Player createPlayer(int id) {
		//use StringGenerator.getRandomFirstName, getRandomFirstName, getRandomPositionOnField
		Person p = new Player(id, StringGenerator.getRandomFirstName(), StringGenerator.getRandomLastName(), StringGenerator.getRandomFieldPositions());
		return (Player) p;
	}
	
	public static void print(ArrayList<Person>persons) {
		for(Person p : persons) {
			System.out.println(p.toString());
		}
	}
	
	public static HashMap<String,ArrayList<Player>> findAllPlayersForPosition(Team t){
		ArrayList<Person> players = t.getPerson();
		HashMap<String, ArrayList<Player>> playerPositions = new HashMap<>();
		
		for(Person p : players) {
			if(p instanceof Player) {
				String position = ((Player) p).getPositionOnField();
				if(playerPositions.containsKey(position)) {
					ArrayList<Player> listPlayers = playerPositions.get(position);
					listPlayers.add((Player) p);
					playerPositions.put(position, listPlayers);
					
				} else {
					ArrayList<Player> listPlayers = new ArrayList<Player>();
					listPlayers.add((Player) p);
					playerPositions.put(position, listPlayers);
				}
				
			}
		}
		return playerPositions;
	}
	
	public static void printMap(HashMap<String,ArrayList<Player>> positionMap) {
		for(HashMap.Entry<String, ArrayList<Player>> entry : positionMap.entrySet()) {
			System.out.println("----------------------------");
			System.out.println("Position: " + entry.getKey());
			for(Player p : entry.getValue()) {
				System.out.println("\t" + p.toString());
			}
		}
	}

}

