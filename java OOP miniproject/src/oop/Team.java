package oop;

import java.util.ArrayList;

public class Team {
private int id;
private String teamName;

Team(int id, String teamName) {

	this.id = id;
	this.teamName = teamName;
	this.setPerson(new ArrayList<Person>());
}
public Team() {
}
private ArrayList<Person>person;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getTeamName() {
	return teamName;
}

public void setTeamName(String teamName) {
	this.teamName = teamName;
}
public ArrayList<Person> getPerson() {
	return person;
}
public void setPerson(ArrayList<Person> person) {
	this.person = person;
}


}
