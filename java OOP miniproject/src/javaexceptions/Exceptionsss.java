package javaexceptions;

import java.util.Scanner;

public class Exceptionsss {
	/*
	 * Don't change the main class, use start application to perform the logic.
	 * a) Create the class NotCorrectCCNException, and make sure that this is
	 * a custom exception. Create a constructor where you call the super constructor
	 * with message "Not a credit card number"
	 * b) Ask the user for a credit card number, use the scanner and the method receiveACreditCardNumber(). 
	 * the length of the ccn is 12 digits (use isCorrectNumber) , make sure this is correct, else throw a NotCorrectCCNException.
	 * 	
	 */
	
	private static  Scanner sc = new Scanner(System.in);
	public static void main(String[] args) {
		startApplication();
	}
	
	public  static boolean isCorrectNumber(String ccn) throws NotcorrectCCNException  {
		
		if(ccn.matches("^[0-9]{12}$")) {
		    ccn = ccn.substring(ccn.length()-2);
		    System.out.println(ccn);
		   
		return true;
		}
	throw new NotcorrectCCNException();
	}
	
	
	public static void startApplication() {
		System.out.println("Give your credit card number: ");
		String ccn = sc.nextLine();
		try {
			isCorrectNumber(ccn);
			System.out.println("Valid ccn number");
		} catch (NotcorrectCCNException e ) {
			System.err.println(e.getMessage());
		}
	}
}


