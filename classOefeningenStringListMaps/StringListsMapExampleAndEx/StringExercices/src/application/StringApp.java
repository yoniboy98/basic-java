package application;

import java.util.Arrays;
import java.util.Scanner;

public class StringApp {
	private static Scanner scan = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		startSearchForPalindrome();
		startSubString();
	}

	public String changeSentence(String stringToChange, String[] changes,String[] wordsForChange) {
		String result = stringToChange;
		int count = 0;
		for(String s : changes) {
			result = result.replace(s, wordsForChange[count]);
			count++;
		}
		return result;
	}

	public static void startStringSequenceToArray() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Geef een lijst van woorden gescheiden door een komma");
		String words = scanner.nextLine();
		String[] splitted = buildStringArray(words);
		printStringArrayReverse(splitted);

	}

	public static String[] buildStringArray(String s) {
		String[] ar = s.split(",");
		return ar;
	}

	public static void printStringArrayReverse(String[] words) {
		Arrays.sort(words);
		System.out.println("Print alphabetically : ");
		for (String s : words) {
			System.out.println(s);
		}
	}

	public static void startSubString() {

		System.out.println("Geef een zin?");
		String sentence = scan.nextLine();
		System.out.println("Geef een woord waarvan je te weten wil komen of het voorkomt in vorige zin?");
		String controlWord = scan.nextLine();
		if (checkIfSentenceContainsWord(sentence, controlWord)) {
			System.out.println(controlWord + " komt voor in de zin: " + sentence);
			scan.close();
			scan.close();
		}

	}

	public static boolean checkIfSentenceContainsWord(String sentence, String word) {
		return sentence.contains(word);
	}

	public static void startSearchForPalindrome() {
		System.out.println("Geef een woord?");
		String s = scan.nextLine();
		if (stringIsPalindrome(s)) {
			System.out.println(s + " is een palindrome");
		} else {
			System.out.println(s + " is geen palindrome");
		}
	}

	public static boolean stringIsPalindrome(String s) {
		String reverse = "";
		for (int i = s.length() - 1; i >= 0; i--) {
			reverse += s.charAt(i);
		}
		return s.equalsIgnoreCase(reverse);
	}

}
