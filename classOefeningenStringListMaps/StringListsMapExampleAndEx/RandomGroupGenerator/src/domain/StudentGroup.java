package domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class StudentGroup {
	
	//Map where all the students should be ordered in groups
	private Map<Integer,List<String>> studentNames;
	
	public StudentGroup() {
		//new instance of a hashmap
		this.studentNames = new HashMap<>();
	}
	
	//TODO 
	public Map<Integer, List<String>> createStudentsGroup(int numberOfGroups){
		//this list should be ordered in a map by group
		List<String> students = this.getRandomOrderedStudentList();
		//check that the size of the list can be divided equally by numberOfGroups replace true
		//for example : numberOfGroups = 5 (than there are 5 groups with each 3 students)
		if(true) {
			//take the number of students in each group (use the example, should be 3 students each group)
			//replace studentsInGroup with the correct calculation (for example : 15 / 5 = 3)
			int studentsInGroup = 0;
			//for creating sublists we need a from index and a to index
			int from = 0;
			int to = studentsInGroup;
			//loop over the number of groups that we need
			for(int i = 0;i<numberOfGroups;i++) {
				//create a sublist from the students, you get this line for free
				//use it wisely
				List<String> group = (List<String>) students.subList(from,to);
				//put the group as value in the map, and make the iteration (step in loop) 
				//as the key for your HashMap (info see oracle page)
				//https://docs.oracle.com/javase/9/docs/api/java/util/HashMap.html
				
				//TODO put it in the map
				
				//replace from and to so that the next group can be created
				from = to;
				to = from+studentsInGroup;
			}
		}
		return this.studentNames;
	}
	
	//TODO
	private List<String> getRandomOrderedStudentList(){
		//Get the static list from Students, and shuffle them 
		//(Tip : see the Collections Interface if there is a method for doing this)
		//https://docs.oracle.com/javase/7/docs/api/java/util/Collections.html
		//return should be replaced
		return new ArrayList<String>();
	}

	public void printList() {
		for(Entry<Integer, List<String>> entry : this.studentNames.entrySet()) {
			System.out.println("Group "+(entry.getKey()+1));
			for(String s : entry.getValue()) {
				System.out.println(s);
			}
			System.out.println();
		}
	}

}
