package domain;

public class RandomGroupApp {

	//Don't change this, try to run it after completing the tasks.
	public static void main(String[] args) {
		StudentGroup studentGroup = new StudentGroup();
		studentGroup.createStudentsGroup(5);
		studentGroup.printList();
	}
//If you run the app after completing all the steps, you should get something like this
//	Group 1
//	Esnol Maxime
//	Coeckelbergh Alexander
//	Lens Arne
//
//	Group 2
//	Van Malder Devin
//	G�l Seda
//	Hesters Stijn
//
//	Group 3
//	Ahriga Mouhsine
//	Vindelinckx Yoni
//	Wauters Arne
//
//	Group 4
//	El Amrani Adel
//	Sunaert Yorick
//	Van Caekenberghe Tim
//
//	Group 5
//	Buelens Jeroen
//	Lemrabet Youssef
//	Tehraoui Illyas
}
