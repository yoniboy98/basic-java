package exercises;

import java.util.Scanner;
import java.util.InputMismatchException;

public class UserInputMismatchEx {

	private static Scanner scan;

	public static void main(String[] args) {
		// TODO 2.c vang de nodige exceptions of die door de methode startApp
		// aangeduid worden als checked Exception
try {
		startApp();
}catch (NumberUserInputException e) {
	e.printStackTrace();
}
	}
	// TODO ex.1 Wanneer de gebruiker geen getal ingeeft zal er iets gebeuren
	// probeer de exception te behandelen en geef de gebruiker de
	// kans om opnieuw een getal in te geven.
	private static void startApp() throws NumberUserInputException{
		try {
			scan = new Scanner(System.in);

			System.out.println("Geef een getal tussen 0 en 10 ");

			int i = scan.nextInt();
			if (i < 0 || i > 10) {
				throw new NumberUserInputException();
			} else {
				System.out.println("het getal is " + i);
			}
			System.out.println("het getal is " + i);
		} catch (Exception e) {
			System.out.println(e);
			System.out.println("geef je getal juist in");
			int i = scan.nextInt();
			System.out.println("het getal is = " + i);
		}
	}
}
