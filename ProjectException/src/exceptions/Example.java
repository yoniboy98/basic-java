package exceptions;

public class Example {
	
	private static void doSomething() throws MyCrazyException {
		System.out.println("doSomething");
		throw new MyCrazyException("My crazy exception is thrown");
	}
	
	public static void main(String[] args) {
		
		try {
			doSomething();
		} catch (MyCrazyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e);
		}
	}
	
}
