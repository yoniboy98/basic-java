package MapsANDLists;
import java.util.*;

public class Iterators {

	public static void main (String[]args) {
		ArrayList<String> names = new ArrayList<String>();
		names.add("mark");
		names.add("tom");
		names.add("jack");
		names.add("johan");
		names.add("aaaaah");
		names.add("willy");
		
		ListIterator<String> listnames = names.listIterator();
		while(listnames.hasNext()) 
			System.out.println(listnames.next());
		
		System.out.println("");System.out.println("");
		
		while(listnames.hasPrevious())
			System.out.println(listnames.previous());
	}
}
