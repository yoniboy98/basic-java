package MapsANDLists;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;

public class ConvertList {

	public static void main(String[] args) {
		String[] array = {"hey","hoi","jahallo"};
		LinkedList<String> thelist = new LinkedList<String>(Arrays.asList(array));
		
		thelist.add("oke");
		thelist.add("fire");
		
		//convert back to an array 
		
		array = thelist.toArray(new String[thelist.size()]);
		for(String x : array)
			System.out.printf("%s ",x);
		

	}

	
	
}
