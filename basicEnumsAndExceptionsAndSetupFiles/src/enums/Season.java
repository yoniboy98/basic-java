package enums;

import java.util.Scanner;

public enum Season {
	REGEN("20% regen", "10% zon", "20% bewolkt"), BEWOLKT("10% regen", "20% zon", "20% bewolkt"),
	ZON("5% regen", "15% zon", "25% bewolkt");

	private final String regen;
	private final String zon;
	private final String bewolkt;

	Season(String winter, String zomer, String bewolkt) {
		this.regen = winter;
		this.zon = zomer;
		this.bewolkt = bewolkt;
	}

	public String getWinter() {
		return regen;
	}

	public String getZomer() {
		return zon;
	}

	public String getBewolkt() {
		return bewolkt;
	}

}
